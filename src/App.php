<?php

class App
{
    /**
     * @var \Soen\Container\Application
     */
    public static $app;
    public static $assembly;
    public static function getContext($id = null){
        return self::$app->context;
    }
    
    public static function getComponent($id){
        return self::$app->context->getComponent($id);
    }

    /**
     * 获取session
     * @return \Soen\Session\Session
     */
    public static function session(){
        /**
         * @var \Soen\Session\Session $session
         */
        $session = self::$app->context->getComponent('session', false);
        $request = self::getContext()->getContextData('request');
        $response = self::getContext()->getContextData('response');
        $session->start($request, $response);
        return $session;
    }

    /**
     * 获取redis
     * @return \Soen\Redis\Pool\Driver
     */
    public static function redis(){
        /**
         * @var \Soen\Redis\Pool\Driver $redis
         */
        $redis = self::$app->context->getComponent('redis');
        return $redis;
    }

    /**
     * 获取logger
     * @return \Soen\Log\Logger
     */
    public static function log () {
        /**
         * @var Soen\Log\Logger $logger
         */
        $logger = self::$app->context->getComponent('log');
        return $logger;
    }

    /**
     * @return \Soen\Event\EventDispatcher
     */
    public static function event () {
        /**
         * @var \Soen\Event\EventDispatcher $event
         */
        $event = self::$app->context->getComponent('event');
        return $event;
    }


}